import { shallowMount, mount } from '@vue/test-utils'
import emailjs from 'emailjs-com';
import FeedbackForm from '@/components/FeedbackForm.vue'
jest.mock('emailjs-com')

describe('FeedbackForm.vue', () => {
  
  it('renders a form', () => {
    const wrapper = shallowMount(FeedbackForm)
    expect(wrapper.find('.container')).not.toBeNull
  })
  it('renders a successfully sent message', async () => {
    const wrapper = shallowMount(FeedbackForm)
    await wrapper.setData({email_sent_status: true})
    const title = wrapper.find('h1')
    expect(title.text()).toContain('Thank you')
  })
  it('should send method and fail', async () => {
    let response = await emailjs.sendForm('test', 'test')
    expect(response).toBeUndefined
  })
  it('should send method Successfully', async () => {
    let templateParams = {
      name: 'am',
      user_email: 'am@gmail.com',
      message: 'good'
    }
    let response = await emailjs.sendForm('service_lgmwipw', 'template_gzg6muc', templateParams,  'user_Br7EJ1OYKmCWTdtNpgbJC')
    expect(response).toBeDefined
  })
  // it('should fails when email pattern is wrong', () => {
  //   const text = "email@gmail.com";
  //   const expression = /.+@.+/
  //   const wrapper = mount(FeedbackForm);
  //   expect(wrapper.find('#user_email').setValue(text)).toMatch(expression)
  //   // expect(wrapper.attributes("type")).toBe(type);
  // })

})
